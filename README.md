# Discogs

A cli for searching releases on discogs. This is a helper tool I made for my playlist project, now making it public so that anyone can use.

### Install

`$ npm install`

### config.js

- discogs.key: consumer key
- discogs.secret: consumer secret

To generate your consumer key and secret, first you need a a discogs account. Once you have the account, go to the `Settings > Developers > Create an application`


### Usage

- To get a random release, just run the script without any params. eg: `$ node discogs.js`

- To get a specific release, run the script with release_id as param. eg: `$ node discogs.js 6093336`

- To get the HTML scaffolding for the album use the flag `-t`. And for selecting tracks, use track no seperated with commas. eg: `$ node discogs.js 6093336 -t 1,2`

